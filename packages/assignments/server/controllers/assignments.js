'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Assignments = mongoose.model('Assignments'),
    _ = require('lodash');


exports.curriculumAssignmentCheck = function(req, res) {
    var curriculumId = req.body.curriculumId,
        edClass = req.body.edClass;

    Assignments.findOne({ curriculumId: curriculumId, "edClass._id": edClass._id }).exec(function(err, assignment) {
        if (err) res.send(500, err);

        if (assignment) {
            res.jsonp( { exists: true } );
        } else {
            res.jsonp( { exists: false });
        }
    });
};
/**
 * Find teacher by id
 */
exports.assignments = function(req, res, next, id) {
    Assignments.load(id, function(err, assignment) {
        if (err) return next(err);
        if (!assignment) return next(new Error('Failed to load assignment ' + id));
        req.assignment = assignment;
        next();
    });
};

/**
 * Create assignment
 */
exports.createAssignment = function(req, res) {
    var assignment = new Assignments(req.body);
    assignment.user = req.user;

    assignment.save(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                assignment: assignment
            });
        } else {
            res.jsonp(assignment);

      }
    });
};

/**
 * Update assignment
 */
exports.update = function(req, res) {

    var assignment = req.assignment;
    assignment = _.extend(assignment,
      {responses:req.body.responses,
      completed:true
      });

    //console.log(assignment)
    assignment.save(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                assignment: assignment
            });
        } else {
            res.jsonp(assignment);
        }
    });
};

/**
 * Delete assignment
 */
exports.destroy = function(req, res) {
    var assignment = req.assignment;

    assignment.remove(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                assignment: assignment
            });
        } else {
            res.jsonp(assignment);
        }
    });
};

/**
 * Show assignment
 */
exports.show = function(req, res) {
    res.jsonp(req.assignment);
};

/**
 * List assignment
 */
exports.all = function(req, res) {
    Assignments.find().sort('-created').populate('user', 'name username').exec(function(err, assignments) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(assignments);
        }
    });
};
