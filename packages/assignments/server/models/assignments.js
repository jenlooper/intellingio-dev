'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * AssignmentSchema Schema
 */
var AssignmentSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    curriculumId: {
        type: String,
        default: '',
        trim: true
    },
    studentId: {
        type: String,
        default: '',
        trim: true
    },
    curriculumType: {
        type: String,
        default: '',
        trim: true
    },
    curriculumTypeId: {
        type: String,
        default: '',
        trim: true
    },
    curriculumName: {
        type: String,
        default: '',
        trim: true
    },
    edClass: {
    },
    responses: {
    },
    completed:{
        type: Boolean,
        default: false,
        trim: true
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

/**
 * Validations
 */
/*AssignmentSchema.path('curriculum').validate(function(curriculum) {
    return curriculum.length;
}, 'Curriculum cannot be blank');
*/

/**
 * Statics
 */
AssignmentSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).populate('user', 'name username').exec(cb);
};

mongoose.model('Assignments', AssignmentSchema);
