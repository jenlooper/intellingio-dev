'use strict';

var am = require('../controllers/assignments');

// Article authorization helpers
var hasAuthorization = function(req, res, next) {
    if (!req.user.isAdmin && req.assignments.user.id !== req.user.id) {
        return res.send(401, 'User is not authorized');
    }
    next();
};

module.exports = function(Assignments, app, auth) {

    app.route('/assignment')
        .get(auth.requiresLogin, hasAuthorization, am.all)
        //.put(auth.requiresLogin, hasAuthorization, am.update)
        .post(auth.requiresLogin, hasAuthorization, am.createAssignment);
    app.route('/assignment/:assignmentId')
        .get(auth.requiresLogin, hasAuthorization, am.show)
        .put(auth.requiresLogin, hasAuthorization, am.update)
        .delete(auth.requiresLogin, hasAuthorization, am.destroy);
    app.route('/assignment/1/:assignmentId')
        .put(auth.requiresLogin, hasAuthorization, am.update);
    app.route('/assignment/2/:assignmentId')
        .put(auth.requiresLogin, hasAuthorization, am.update);
    app.route('/assignment/3/:assignmentId')
        .put(auth.requiresLogin, hasAuthorization, am.update);
    app.route('/assignment/4/:assignmentId')
        .put(auth.requiresLogin, hasAuthorization, am.update);
    app.route('/assignmentsCheck')
        .post(auth.requiresLogin, hasAuthorization, am.curriculumAssignmentCheck);

    app.param('assignmentId', am.assignments);
    //app.param('curriculumId', am.assignments);
};
