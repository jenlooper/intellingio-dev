'use strict';

angular.module('mean').factory('Assignment', ['$resource',
	function($resource) {
		return $resource('assignment/:curriculumId', {
			curriculumId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
