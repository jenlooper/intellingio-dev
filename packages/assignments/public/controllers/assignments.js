'use strict';

angular.module('mean').controller('AssignmentController', ['$scope', '$stateParams', '$location', 'Global', 'Assignment','Users',
    function($scope, $stateParams, $location, Global, Assignment,Users) {
        $scope.global = Global;

        $scope.hasAuthorization = function(assignment) {
            if (!assignment || !assignment.user) return false;
            return $scope.global.isAdmin || assignment.user._id === $scope.global.user._id;
        };


        $scope.remove = function(assignment) {
            if (assignment) {
                assignment.$remove();

                for (var i in $scope.assignments) {
                    if ($scope.assignments[i] === assignment) {
                        $scope.assignments.splice(i, 1);
                    }
                }
            } else {
                $scope.assignment.$remove(function(response) {
                    //$location.path('classManagement/');
                });
            }
        };

        $scope.update = function(isValid) {
            if (isValid) {
                var assignment = $scope.assignment;

                if (!assignment.updated) {
                    assignment.updated = [];
                }
                assignment.updated.push(new Date().getTime());
                assignment.$update(function() {
                    $location.path('/students/assignments');
                });
            } else {
                $scope.submitted = true;
            }
        };

        $scope.find = function() {
            Assignment.query(function(assignments) {
                $scope.assignments = assignments;
            });

        };


        $scope.findMy = function() {
            Assignment.query(function(assignments) {
                $scope.assignments = assignments.filter(function(work){
                    return(work.studentId.indexOf($scope.global.user._id) !== -1);
                });
            });
        };

        $scope.findOne = function() {
            Assignment.get({
                assignmentId: $stateParams.curriculumId
            }, function(assignment) {
                $scope.assignment = assignment;
            });

        };


        $scope.findOneAssignment = function() {
          Assignment.query(function(assignment) {
              var assignment = assignment.filter(function(work){
                  return(work.curriculumId.indexOf($stateParams.curriculumId) !== -1);
              });
              $scope.assignment = assignment[0];

          });
        };

      //take a curriculum element and assign a class to it. One curriculum piece can have several classes associated to it
       $scope.addRemoveClass = function(curriculumId,classId) {

            var curriculumIdx = $scope.assignments.indexOf(curriculumId);

            //if this curriculum item exists and does not have this classId, update it
            if ($scope.assignments[curriculumIdx].edclasses.indexOf(classId) === -1){
                $scope.assignments[curriculumIdx].edclasses.push(classId);
                $scope.assignment = $scope.assignment[classId];
                $scope.update(true);
            }
            else{
                $scope.assignments[curriculumIdx].edclasses.splice(classId,1);
                $scope.assignment = $scope.assignments[classId];
                $scope.update(true);
            }


        };


        $scope.complete = function(){
            $location.path('/teachers');
        };
    }
]);
