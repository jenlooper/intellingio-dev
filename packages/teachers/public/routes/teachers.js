'use strict';

//Setting up route
angular.module('mean').config(['$stateProvider',
    function($stateProvider) {
        // Check if the user is connected
        var checkLoggedin = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {

                // Authenticated
                if (user !== '0') $timeout(deferred.resolve);

                // Not Authenticated
                else {
                    $timeout(deferred.reject);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

        // states for my app
        $stateProvider
            .state('teacher home', {
                url: '/teachers',
                templateUrl: 'teachers/views/index.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })

            .state('curriculum', {
                url: '/teachers/curriculum',
                templateUrl: 'teachers/views/curriculum.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('classes', {
                url: '/teachers/classes',
                templateUrl: 'teachers/views/classes.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('students', {
                url: '/teachers/students',
                templateUrl: 'teachers/views/invite.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('list students', {
                url: '/teachers/invited',
                templateUrl: 'teachers/views/students.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('scores', {
                url: '/teachers/scores',
                templateUrl: 'teachers/views/scores.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('score_detail', {
                url: '/students/:studentId',
                templateUrl: 'teachers/views/score_detail.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('my-home', {
                url: '/teachers/home',
                templateUrl: 'teachers/views/home.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            });
    }
]);
