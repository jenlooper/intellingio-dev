'use strict';

angular.module('mean').controller('TeachersController', ['$scope', '$http','$stateParams', '$location', 'Global', 'Teachers',
    function($scope, $http, $stateParams, $location, Global, Teachers) {
        $scope.global = Global;


        $scope.hasAuthorization = function(teacher) {
            if (!teacher || !teacher.user) return false;
            return $scope.global.isAdmin || teacher.user._id === $scope.global.user._id;
        };

        
       
    }
]);
