'use strict';

/**
* mean Module
*
* Audio Recorder Directive
*/
angular.module('mean').directive('audioRecorder', ['$timeout','$http','$stateParams', function($timeout, $http, $stateParams) {
	return {
		restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
		scope: {
			ngModel: '=',
			message: '=',
			recording: '=',
			update: '&'
		},
		link: function(scope, iElm, iAttrs, controller) {
			var updateHandler = scope.update();
			var audio_context;
				try {
				// webkit shim
				window.AudioContext = window.AudioContext || window.webkitAudioContext;
				navigator.getUserMedia = ( navigator.getUserMedia ||
					navigator.webkitGetUserMedia ||
					navigator.mozGetUserMedia ||
					navigator.msGetUserMedia);
				window.URL = window.URL || window.webkitURL;

				audio_context = new window.AudioContext();
			} catch (e) {
				//console.log(e);
			}

			var recorderObject;

			iElm.children('.start').on('click', function() {
				scope.ngModel = undefined;
				scope.recording = true;
				iElm.data('recorderObject', recorderObject);

				navigator.getUserMedia({audio: true}, function(stream) {
					recorderObject = new MP3Recorder(audio_context, stream);
				 	recorderObject.start();
				 	scope.message = 'Recording';
				}, function(e) {
					//console.log('e' + e);
				});
			});
			iElm.children('.stop').on('click', function() {
		 		recorderObject.stop();

				var el = document.getElementById("audio-tag");
				el.load();

		 		scope.message = 'Converting Recording';

				recorderObject.exportMP3(function(base64_mp3_data) {

					var data = base64_mp3_data;

					scope.message = 'Saving Recording';
					var recording_id = $stateParams.curriculumId;
					$http.post('teachers/audioupload', { data: data, recording_id: recording_id}).then(
						function (response) {
							scope.message = 'Complete';
							scope.ngModel = response.data.filePath;

							$timeout(function() {
								scope.message = undefined;
								scope.recording = false;
								updateHandler(true);
							},3000);

						});
				});


			});

		}
	};
}]);
