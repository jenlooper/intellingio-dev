'use strict';

//Teachers service used for teachers REST endpoint
angular.module('mean').factory('Teachers', ['$resource',
	function($resource) {
		return $resource('teachers/:teacherId', {
			teacherId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);