'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Teacher = mongoose.model('Teacher'),
    _ = require('lodash'),
     fs = require('fs'),
    config = require('meanio').loadConfig();


/**
 * Find teacher by id
 */
exports.teacher = function(req, res, next, id) {
    Teacher.load(id, function(err, teacher) {
        if (err) return next(err);
        if (!teacher) return next(new Error('Failed to load teacher ' + id));
        req.teacher = teacher;
        next();
    });
};

/**
 * Create a student for a teacher
 */
exports.create = function(req, res) {
    var teacher = new Teacher(req.body);
    teacher.user = req.user;

    teacher.save(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                teacher: teacher
            });
        } else {
            res.jsonp(teacher);

      }
    });
};

/**
 * Update a teacher
 */
exports.update = function(req, res) {
    var teacher = req.teacher;

    teacher = _.extend(teacher, req.body);

    teacher.save(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                teacher: teacher
            });
        } else {
            res.jsonp(teacher);
        }
    });
};

/**
 * Delete teacher
 */
exports.destroy = function(req, res) {
    var teacher = req.teacher;

    teacher.remove(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                teacher: teacher
            });
        } else {
            res.jsonp(teacher);
        }
    });
};

/**
* Upload Audio
*/
exports.audioUpload = function(req, res) {
    var decoded_audio = new Buffer(req.body.data, 'base64').toString('binary');
    // console.log(decoded_audio);
    var path = config.root + '/packages/system/public/assets/files/',
        filename = req.body.recording_id + '.mp3';

    fs.writeFile(path + filename, decoded_audio, 'binary',  function(err) {
        if (err){
            res.json(403, err);
          }
        else{
        res.json(200, { filePath: '/system/assets/files/' + filename });
      }
    });
};

/**
 * Show quiz
 */
exports.show = function(req, res) {
    res.jsonp(req.teacher);
};

/**
 * List quiz
 */
exports.all = function(req, res) {
    Teacher.find().sort('-created').populate('user', 'name username').exec(function(err, teachers) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(teachers);
        }
    });
};
