'use strict';

var teachers = require('../controllers/teachers');

// Teacher authorization helpers
var hasAuthorization = function(req, res, next) {
    if (!req.user.isAdmin && req.teacher.user.id !== req.user.id) {
        return res.send(401, 'User is not authorized');
    }
    next();
};

module.exports = function(Teachers, app, auth) {

    app.route('/teachers')
        .get(auth.requiresLogin, hasAuthorization, teachers.all)
        .post(auth.requiresLogin, hasAuthorization, teachers.create);
    app.route('/teachers/:teacherId')
        .get(auth.requiresLogin, hasAuthorization, teachers.show)
        .put(auth.requiresLogin, hasAuthorization, teachers.update)
        .delete(auth.requiresLogin, hasAuthorization, teachers.destroy);
    app.route('/teachers/audioupload')
        .post(auth.requiresLogin, hasAuthorization, teachers.audioUpload);
    
    app.param('teacherId', teachers.teacher);
};
