'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Teachers = new Module('teachers');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Teachers.register(function(app, auth, database) {

    //We enable routing. By default the Package Object is passed to the routes
    Teachers.routes(app, auth, database);

    //We are adding a link to the main menu for all authenticated users
    Teachers.menus.add({
        'roles': ['teacher'],
        'title': 'Curriculum',
        'icon': 'glyphicon glyphicon-pencil',
        'link': 'curriculum',
        menu:'teacher'
    });

    Teachers.menus.add({
        'roles': ['teacher'],
        'title': 'My Classes',
        'icon': 'glyphicon glyphicon-bell',
        'link': 'classes',
        menu:'teacher'
    });

    Teachers.menus.add({
        'roles': ['teacher'],
        'title': 'Pending',
        'icon': 'glyphicon glyphicon-user',
        'link': 'list students',
        menu:'teacher'
    });

    Teachers.menus.add({
        'roles': ['teacher'],
        'title': 'Invite',
        'icon': 'glyphicon glyphicon-envelope',
        'link': 'students',
        menu:'teacher'
    });

    Teachers.menus.add({
        'roles': ['teacher'],
        'title': 'Scores',
        'icon': 'glyphicon glyphicon-ok',
        'link': 'scores',
        menu:'teacher'
    });

    Teachers.menus.add({
        'roles': ['authenticated,administration'],
        'title': 'User Management',
        'icon': 'glyphicon glyphicon-user',
        'link': 'users',
        menu:'admin'
    });

    Teachers.menus.add({
        'roles': ['teacher'],
        'title': 'Home',
        'icon': 'glyphicon glyphicon-home',
        'link': 'my-home',
        menu:'teacher'
    });
    

    //Articles.aggregateAsset('js','/system/services/menus.js',{group:'footer',absolute:true, weight:-9999});
    Teachers.aggregateAsset('js', 'test.js', {
        group: 'footer',
        weight: -1
    });


    /*
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    Articles.settings({'someSetting':'some value'},function (err, settings) {
      //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    Articles.settings({'anotherSettings':'some value'});

    // Get settings. Retrieves latest saved settings
    Articles.settings(function (err, settings) {
      //you now have the settings object
    });
    */
    Teachers.aggregateAsset('css', 'teachers.css');

    return Teachers;
});
