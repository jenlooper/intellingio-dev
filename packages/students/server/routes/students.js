'use strict';

var students = require('../controllers/students');

// Student authorization helpers
var hasAuthorization = function(req, res, next) {
    if (!req.user.isAdmin && req.student.user.id !== req.user.id) {
        return res.send(401, 'User is not authorized');
    }
    next();
};

module.exports = function(Students, app, auth) {

    app.route('/students')
        .get(auth.requiresLogin, hasAuthorization, students.all)
        .post(auth.requiresLogin, hasAuthorization, students.create);
    app.route('/students/:studentId')
        .get(auth.requiresLogin, hasAuthorization, students.show)
        .put(auth.requiresLogin, hasAuthorization, students.update)
        .delete(auth.requiresLogin, hasAuthorization, students.destroy);


    app.param('studentId', students.student);
};
