'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ResponseSchema = new Schema({
    question: {
        type: String,
        default: '',
        trim: true
    },
    answer: {
        type: String,
        default: '',
        trim: true
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});
/**
 * Student Schema
 */
var StudentSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    studentId: {
        type: String,
        default: '',
        trim: true
    },
    curriculumId: {
        type: String,
        default: '',
        trim: true
    },
    curriculumName: {
        type: String,
        default: '',
        trim: true
    },
    score: {
        type: String,
        default: '',
        trim: true
    },
    complete: {
      type: Boolean,
      default: false,
      trim:true      
    },
    responses: [ResponseSchema],
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

/*var InvitedStudentSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    email: {
        type: String,
        default: '',
        trim: true
    },
    classcode: {
        type: String,
        default: '',
        trim: true
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});
*/


/**
 * Statics
 */
StudentSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).populate('user', 'name username').exec(cb);
};

mongoose.model('Student', StudentSchema);
//mongoose.model('InvitedStudent', InvitedStudentSchema);

