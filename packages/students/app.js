'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Students = new Module('students');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Students.register(function(app, auth, database) {

    //We enable routing. By default the Package Object is passed to the routes
    Students.routes(app, auth, database);

    //We are adding a link to the main menu for all authenticated users
    Students.menus.add({
        'title': 'Assignments',
        'link': 'students assignments',
        'icon': 'glyphicon glyphicon-paperclip',
        'roles': ['student'],
        menu: 'student'
    });

    Students.menus.add({
        'title': 'Progress',
        'link': 'students scores',
        'icon': 'glyphicon glyphicon-ok',
        'roles': ['student'],
        menu: 'student'
    });

    Students.menus.add({
        'title': 'Feedback',
        'link': 'students feedback',
        'icon': 'glyphicon glyphicon-bullhorn',
        'roles': ['student'],
        menu: 'student'
    });
    Students.menus.add({
        'title': 'Home',
        'link': 'students home',
        'icon': 'glyphicon glyphicon-home',
        'roles': ['student'],
        menu: 'student'
    });


    Students.aggregateAsset('js', 'test.js', {
        group: 'footer',
        weight: -1
    });


    /*
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    Articles.settings({'someSetting':'some value'},function (err, settings) {
      //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    Articles.settings({'anotherSettings':'some value'});

    // Get settings. Retrieves latest saved settings
    Articles.settings(function (err, settings) {
      //you now have the settings object
    });
    */
    Students.aggregateAsset('css', 'students.css');

    return Students;
});
