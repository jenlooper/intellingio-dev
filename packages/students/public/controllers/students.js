'use strict';



angular.module('mean').controller('StudentsController', ['$scope', '$http','$stateParams', '$location', 'Global', 'Students','Assignment',
    function($scope, $http, $stateParams, $location, Global, Students, Assignment) {
        $scope.global = Global;
        $scope.package = {
            name: 'students'
        };

        $scope.hasAuthorization = function(students) {
            if (!students || !students.user) return false;
            return $scope.global.isAdmin || students.user._id === $scope.global.user._id;
        };

        $scope.create = function(isValid) {

            if (isValid) {
                var student = new Students({
                    studentId: $scope.global.user._id,
                    curriculumId: $scope.assignment.curriculumId,
                    curriculumName: $scope.assignment.curriculumName,
                    complete: true,
                    responses: $scope.assignment.content
                });
                student.$save(function(response) {
                    $location.path('students/assignments');
                });

                this.studentId = '';
                this.curriculumId = '';
                this.curriculumName = '';
                this.complete = false;
                this.responses = [];
                $scope.success = 'Exercise completed!';

            } else {
                $scope.submitted = true;
                $scope.error = 'There was a problem submitting your answers, please try again.';

            }
        };       
        
        $scope.update = function(isValid,student) {
            if (isValid) {
                /*console.log(student);
                var student = $scope.student;
                if (!student.updated) {
                    student.updated = [];
                }
                student.updated.push(new Date().getTime());*/
                student.$update(function() {
                    //$location.path('quizzes/' + quiz._id);
                });
            } else {
                $scope.submitted = true;
            }
        };
        $scope.findOne = function() {
            Students.query(function(students) {
                $scope.students = students.filter(function(scholar){
                    return(scholar.studentId.indexOf($stateParams.studentId) !== -1);
                });

            });
        };

        
        $scope.findMyGrades = function() {
            Students.query(function(students) {
                $scope.grades = students.filter(function(grade){
                    return(grade.studentId.indexOf($scope.global.user._id) !== -1);
                });

            });
        };


        $scope.findMyAssignments = function() {
            Students.query(function(assignments) {
                $scope.assignments = assignments;
                assignments = assignments.filter(function(work){
                    return(work.edClass.students.indexOf($scope.global.user._id) !== -1);
                });

            });
        };

         $scope.find = function() {
            Students.query(function(students) {
                $scope.students = students;
            });
        };

        $scope.handleDrop = function(item, bin) {
            alert('Item ' + item + ' has been dropped into ' + bin);
        };



    }
]);
