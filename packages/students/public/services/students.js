'use strict';

//Students service used for students REST endpoint
angular.module('mean').factory('Students', ['$resource',
	function($resource) {
		return $resource('students/:studentId', {
			studentId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

