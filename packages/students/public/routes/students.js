'use strict';

//Setting up route
angular.module('mean').config(['$stateProvider',
    function($stateProvider) {
        // Check if the user is connected
        var checkLoggedin = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0') $timeout(deferred.resolve);

                // Not Authenticated
                else {
                    $timeout(deferred.reject);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

        // states for my app
        $stateProvider
            .state('students home', {
                url: '/students/home',
                templateUrl: 'students/views/index.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('students assignments', {
                url: '/students/assignments',
                templateUrl: 'students/views/assignments.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('complete quiz', {
                url: '/assignment/1/:curriculumId',
                templateUrl: 'students/views/quiz.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('complete listeningcomp', {
                url: '/assignment/2/:curriculumId',
                templateUrl: 'students/views/listeningcomp.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('complete comic', {
                url: '/assignment/3/:curriculumId',
                templateUrl: 'students/views/comic.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('complete oral exam', {
                url: '/assignment/4/:curriculumId',
                templateUrl: 'students/views/oralexam.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('students scores', {
                url: '/students/scores',
                templateUrl: 'students/views/scores.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('students feedback', {
                url: '/students/feedback',
                templateUrl: 'students/views/feedback.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            });


    }
]);
