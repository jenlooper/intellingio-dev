'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * QuizElements Schema
 */
var QuizElementsSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    question: {
        type: String,
        default: '',
        trim: true
    },
    answer: {
        type: String,
        default: '',
        trim: true
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});
/**
 * Quiz Schema
 */
var QuizSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    title: {
        type: String,
        default: '',
        schemaKey: 'title',            
        trim: true
    },
    quizelements: [QuizElementsSchema],
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

/**
 * Validations
 */
QuizSchema.path('title').validate(function(title) {
    return title.length;
}, 'Title cannot be blank');

/**
 * Statics
 */
QuizSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).populate('user', 'name username').exec(cb);
};

mongoose.model('Quiz', QuizSchema);
