'use strict';

var quizzes = require('../controllers/quizzes');

// Article authorization helpers
var hasAuthorization = function(req, res, next) {
  if (!req.user.isAdmin && req.quiz.user.id !== req.user.id) {
        return res.send(401, 'User is not authorized');
    }
    next();
};



module.exports = function(Quizzes, app, auth) {

    app.route('/quizzes')
        .get(auth.requiresLogin, hasAuthorization, quizzes.all)
        .post(auth.requiresLogin, hasAuthorization, quizzes.create);
    app.route('/quizzes/:quizId')
        .get(auth.requiresLogin, hasAuthorization, quizzes.show)
        .put(auth.requiresLogin, hasAuthorization, quizzes.update)
        .delete(auth.requiresLogin, hasAuthorization, quizzes.destroy);

    app.param('quizId', quizzes.quiz);
};
