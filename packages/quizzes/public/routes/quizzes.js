'use strict';

//Setting up route
angular.module('mean.quizzes').config(['$stateProvider',
    function($stateProvider) {
        // Check if the user is connected
        var checkLoggedin = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0') $timeout(deferred.resolve);

                // Not Authenticated
                else {
                    $timeout(deferred.reject);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

        // states for my app
        $stateProvider
            .state('all quizzes', {
                url: '/quizzes',
                templateUrl: 'quizzes/views/list.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('create quiz', {
                url: '/quizzes/create',
                templateUrl: 'quizzes/views/create.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('edit quiz', {
                url: '/quizzes/:quizId/edit',
                templateUrl: 'quizzes/views/edit.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('assign quiz', {
                url: '/quizzes/assign/:quizId',
                templateUrl: 'quizzes/views/assign.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('quiz by id', {
                url: '/quizzes/:quizId',
                templateUrl: 'quizzes/views/view.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            });
    }
]);
