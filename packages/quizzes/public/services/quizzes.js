'use strict';

angular.module('mean').factory('Quizzes', ['$resource',
	function($resource) {
		return $resource('quizzes/:quizId', {
			quizId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
