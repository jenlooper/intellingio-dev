'use strict';

angular.module('mean').controller('QuizzesController', ['$scope', '$stateParams', '$location', 'Global', 'Quizzes', '$state',
    function($scope, $stateParams, $location, Global, Quizzes, $state) {
        $scope.global = Global;

        $scope.hasAuthorization = function(quiz) {
            if (!quiz || !quiz.user) return false;
            return $scope.global.isAdmin || quiz.user._id === $scope.global.user._id;

        };


        $scope.create = function(isValid) {
            //just create the base doc
            if (isValid) {
              $scope.qerror = 0;
                var quiz = new Quizzes({
                    title: this.title,
                    quizelements:[]  ///important
                });
                quiz.$save(function(response) {
                    $location.path('quizzes/' + response._id);
                });
                this.title = '';
            } else {
                $scope.submitted = true;
                $scope.qerror = 'Please give this quiz a title.';
            }
        };

        $scope.removeQuizElement = function(quizElement) {
            for (var i in $scope.quiz.quizelements) {
                if ($scope.quiz.quizelements[i] === quizElement) {
                    $scope.quiz.quizelements.splice(i, 1);
                    $scope.update(true);
                }
            }
        };

        $scope.remove = function(quiz) {
            if (quiz) {
                quiz.$remove();

                for (var i in $scope.quizzes) {
                    if ($scope.quizzes[i] === quiz) {
                        $scope.quizzes.splice(i, 1);
                    }
                }
            } else {
                $scope.quiz.$remove(function(response) {
                    $location.path('quizzes');
                });
            }
        };

        $scope.update = function(isValid) {
            if (isValid) {
                var quiz = $scope.quiz;
                if (!quiz.updated) {
                    quiz.updated = [];
                }
                quiz.updated.push(new Date().getTime());
                quiz.$update(function() {
                    $location.path('quizzes/' + quiz._id);
                });
            } else {
                $scope.submitted = true;
            }
        };

        $scope.add = function() {
            $scope.quiz.quizelements.push({
                question: $scope.newQuestion,
                answer: $scope.newAnswer
            });
            $scope.update(true);

            $scope.newQuestion = '';
            $scope.newAnswer = '';
        };

         $scope.find = function() {
            Quizzes.query(function(quizzes) {
                $scope.quizzes = quizzes;
            });
        };

        $scope.findOne = function() {
            Quizzes.get({
                quizId: $stateParams.quizId
            }, function(quiz) {
                $scope.quiz = quiz;
            });
        };

        $scope.findOneAssignment = function() {
            Quizzes.get({
                quizId: $stateParams.curriculumId
            }, function(quiz) {
                $scope.quiz = quiz;
            });
        };

        $scope.edit = function(quiz){

            $scope.quiz = quiz;
            $scope.update(true);
            $location.path('quizzes/' + quiz._id);

        };
        $scope.complete = function(quiz){

            $scope.quiz = quiz;
            $scope.update(true);
            $location.path('/quizzes');

        };
        $scope.returnToPackageHome = function(){
            $location.path('/quizzes');
        };
    }
]);
