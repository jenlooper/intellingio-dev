'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Quizzes = new Module('quizzes');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Quizzes.register(function(app, auth, database) {

    //We enable routing. By default the Package Object is passed to the routes
    Quizzes.routes(app, auth, database);

    /*Quizzes.menus.add({
        'title': 'Quizzes',
        'link': 'all quizzes',
        'icon': 'glyphicon glyphicon-pencil',
        'roles': ['teacher'],
        menu: 'curriculum'
    });*/

    /**
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    Quizzes.settings({
        'someSetting': 'some value'
    }, function(err, settings) {
        //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    Quizzes.settings({
        'anotherSettings': 'some value'
    });

    // Get settings. Retrieves latest saved settigns
    Quizzes.settings(function(err, settings) {
        //you now have the settings object
    });
    */

    return Quizzes;
});
