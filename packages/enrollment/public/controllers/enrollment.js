'use strict';



angular.module('mean').controller('EnrollmentController', ['$scope', '$http','$stateParams', '$location', 'Global', 'Enrollment',
    function($scope, $http, $stateParams, $location, Global, Enrollment) {
        $scope.global = Global;
        
        $scope.hasAuthorization = function(enrollment) {
            if (!enrollment || !enrollment.user) return false;
            return $scope.global.isAdmin || enrollment.user._id === $scope.global.user._id;
        };

        $scope.create = function(isValid) {
           
            if (isValid) {

                
                var enrollment = new Enrollment({
                    email: this.email,
                    classname: $scope.selectedClass.classname,
                    classcode: $scope.selectedClass.classcode
                });
                
                enrollment.$save(function(response) {
                    $location.path('teachers/students');                    
                });


                $http.post('/invite-student', {
                    email: this.email,
                    classname: $scope.selectedClass.classname,
                    classcode: $scope.selectedClass.classcode
                });

                this.email = '';
                this.classname = '';
                this.classcode = '';
                $scope.invitesuccess = 'You have successfully invited this student';
            } else {
                $scope.submitted = true;
                $scope.inviteerror = 'There was a problem inviting this student';
            }
        };

        
        $scope.find = function() {
            Enrollment.query(function(enrollment) {
                $scope.enrollment = enrollment;
            });
        };

        
    }
]);
