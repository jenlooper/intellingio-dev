'use strict';

//Students service used for students REST endpoint
angular.module('mean').factory('Enrollment', ['$resource',
	function($resource) {
		return $resource('enrollment/:enrollmentId', {
			enrollmentId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

