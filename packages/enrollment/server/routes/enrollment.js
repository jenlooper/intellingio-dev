'use strict';

var enrollment = require('../controllers/enrollment');

// Student authorization helpers
var hasAuthorization = function(req, res, next) {
    if (!req.user.isAdmin && req.enrollment.user.id !== req.user.id) {
        return res.send(401, 'User is not authorized');
    }
    next();
};

module.exports = function(Enrollment, app, auth) {

    app.route('/enrollment')
        .get(auth.requiresLogin, hasAuthorization, enrollment.all)
        .post(auth.requiresLogin, hasAuthorization, enrollment.create);
    /*app.route('/enrollment/:enrollmentId')
        .get(auth.requiresLogin, hasAuthorization, enrollment.show)
        .put(auth.requiresLogin, hasAuthorization, enrollment.update)
        .delete(auth.requiresLogin, hasAuthorization, enrollment.destroy);


    app.param('enrollmentId', enrollment.enrollment);*/
};
