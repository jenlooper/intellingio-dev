'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;



var EnrollmentSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    email: {
        type: String,
        default: '',
        trim: true
    },
    classcode: {
        type: String,
        default: '',
        trim: true
    },
    classname: {
        type: String,
        default: '',
        trim: true
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});



/**
 * Statics
 */
EnrollmentSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).populate('user', 'name username').exec(cb);
};

mongoose.model('InvitedStudents', EnrollmentSchema);

