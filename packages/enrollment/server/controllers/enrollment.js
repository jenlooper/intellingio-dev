'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Enrollment = mongoose.model('InvitedStudents'),
    _ = require('lodash');



/**
 * Find enrollment by id
 */
exports.enrollment = function(req, res, next, id) {
    Enrollment.load(id, function(err, enrollment) {
        if (err) return next(err);
        if (!enrollment) return next(new Error('Failed to load enrollment data ' + id));
        req.enrollment = enrollment;
        next();
    });
};

exports.create = function(req, res) {

    var enrollment = new Enrollment(req.body);
    enrollment.user = req.user;

    enrollment.save(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                enrollment: enrollment
            });
        } else {
            res.jsonp(enrollment);
            
      }
    });
};

/**
 * List enrollment
 */
exports.all = function(req, res) {
    Enrollment.find().sort('-created').populate('user', 'name username').exec(function(err, enrollments) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(enrollments);
        }
    });
};
