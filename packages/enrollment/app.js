'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Enrollment = new Module('enrollment');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Enrollment.register(function(app, auth, database) {

    //We enable routing. By default the Package Object is passed to the routes
    Enrollment.routes(app, auth, database);

    //We are adding a link to the main menu for all authenticated users
    

    Enrollment.aggregateAsset('js', 'test.js', {
        group: 'footer',
        weight: -1
    });



    return Enrollment;
});
