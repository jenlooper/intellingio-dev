'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


/**
 * OO Schema
 */
var OnlineOralsSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    paragraph: {
        type: String,
        default: '',
        trim: true
    },
    title: {
        type: String,
        default: '',
        trim: true
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

/**
 * Validations
 */
OnlineOralsSchema.path('title').validate(function(title) {
    return title.length;
}, 'Title cannot be blank');



/**
 * Statics
 */
OnlineOralsSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).populate('user', 'name username').exec(cb);
};

mongoose.model('OnlineOrals', OnlineOralsSchema);
