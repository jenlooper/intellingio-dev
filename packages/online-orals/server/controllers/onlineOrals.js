'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    //fs = require('fs'),
    OnlineOrals = mongoose.model('OnlineOrals'),
    _ = require('lodash');


/**
 * Find paragraph by id
 */
exports.onlineorals = function(req, res, next, id) {
    OnlineOrals.load(id, function(err, onlineorals) {
        if (err) return next(err);
        if (!onlineorals) return next(new Error('Failed to load onlineorals ' + id));
        req.onlineorals = onlineorals;
        next();
    });
};

exports.create = function(req, res) {

    var onlineorals = new OnlineOrals(req.body);

    onlineorals.user = req.user;

    onlineorals.save(function(err) {

        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                onlineorals: onlineorals
            });
        } else {
            res.jsonp(onlineorals);

      }
    });
};


/**
 * Show
 */
exports.show = function(req, res) {

    res.jsonp(req.onlineorals);
};

/**
 * Update
 */
exports.update = function(req, res) {
    var onlineorals = req.onlineorals;

    onlineorals = _.extend(onlineorals, {
        title:req.body.title,
        paragraph:req.body.paragraph
    });

        onlineorals.save(function(err) {
                if (err) {
                    return res.send('users/signup', {
                        errors: err.errors,
                        onlineorals: onlineorals
                    });
                } else {
                    res.jsonp(onlineorals);
                }
            });

};

/**
 * Delete onlineorals
 */
exports.destroy = function(req, res) {

    var onlineorals = req.onlineorals;

    onlineorals.remove(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                onlineorals: onlineorals
            });
        } else {
            res.jsonp(onlineorals);
        }
    });
};

/**
 * List
 */
exports.all = function(req, res) {

    OnlineOrals.find().sort('-created').populate('user', 'name username').exec(function(err, onlineorals) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(onlineorals);
        }
    });
};
