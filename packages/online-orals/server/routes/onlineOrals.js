'use strict';

var oo = require('../controllers/onlineOrals');

var hasAuthorization = function(req, res, next) {
    if (!req.user.isAdmin && req.onlineorals.user.id !== req.user.id) {
        return res.send(401, 'User is not authorized');
    }
    next();
};

module.exports = function(OnlineOrals, app, auth) {

    app.route('/onlineOrals')
    	.get(auth.requiresLogin, hasAuthorization, oo.all)
        .post(auth.requiresLogin, hasAuthorization, oo.create);
    app.route('/onlineOrals/:onlineoralsId')
        .get(auth.requiresLogin, hasAuthorization, oo.show)
        .put(auth.requiresLogin, hasAuthorization, oo.update)
        .delete(auth.requiresLogin, hasAuthorization, oo.destroy);


    app.param('onlineoralsId', oo.onlineorals);
};
