'use strict';

//Setting up route
angular.module('mean').config(['$stateProvider',
    function($stateProvider) {
        // Check if the user is connected
        var checkLoggedin = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0') $timeout(deferred.resolve);

                // Not Authenticated
                else {
                    $timeout(deferred.reject);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

        // states for my app

            $stateProvider
                .state('all online-orals', {
                    url: '/onlineOrals',
                    templateUrl: 'online-orals/views/list.html',
                    resolve: {
                        loggedin: checkLoggedin
                    }
                })
            .state('edit online-orals', {
                url: '/onlineOrals/:onlineoralsId/edit',
                templateUrl: 'online-orals/views/edit.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('create online-orals', {
                url: '/onlineOrals/create',
                templateUrl: 'online-orals/views/create.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('assign online-orals', {
                url: '/onlineOrals/assign/:onlineoralsId',
                templateUrl: 'online-orals/views/assign.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            
            .state('create oo voice', {
                url: '/onlineOrals/voice/:onlineoralsId',
                templateUrl: 'online-orals/views/add-voice.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('student view online-orals assignment', {
                url: '/onlineOrals/students/assignment/4/:onlineoralsId',
                templateUrl: 'online-orals/views/student-view.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            });







    }
]);
