'use strict';

angular.module('mean').controller('OnlineOralsController', ['$scope', '$sce', '$stateParams', '$location', 'Global', 'OnlineOrals','audio',
    function($scope, $sce, $stateParams, $location, Global, OnlineOrals,audio) {
        $scope.global = Global;

        $scope.hasAuthorization = function(onlineorals) {
            if (!onlineorals || !onlineorals.user) return false;
            return $scope.global.isAdmin || onlineorals.user._id === $scope.global.user._id;
        };

        $scope.trustSrc = function(src) {
            return $sce.trustAsResourceUrl(src);
        };
        $scope.play = function(id) {
            audio.play('packages/files/'+id+'/voice_file.wav');
        };
        $scope.pause = function(id) {
            audio.pause('packages/files/'+id+'/voice_file.wav');
        };

        $scope.create = function(isValid) {
            if (isValid) {
              $scope.ooerror = 0;
                var onlineorals = new OnlineOrals({
                    title: this.title
                });
                onlineorals.$save(function(response) {
                    $location.path('onlineOrals/voice/' + response._id);
                });

                this.title = '';
            } else {
              $scope.ooerror = 'Please give a title for this online oral exam.';
                $scope.submitted = true;
            }
        };

        $scope.remove = function(onlineorals) {
            if (onlineorals) {
                onlineorals.$remove();

                for (var i in $scope.onlineorals) {
                    if ($scope.onlineorals[i] === onlineorals) {
                        $scope.onlineorals.splice(i, 1);
                    }
                }
            } else {
                $scope.onlineorals.$remove(function(response) {
                    $location.path('onlineOrals/');
                });
            }
        };


        $scope.update = function(isValid) {
            if (isValid) {
                var onlineorals = $scope.onlineorals;
                if (!onlineorals.updated) {
                    onlineorals.updated = [];
                }
                onlineorals.updated.push(new Date().getTime());
                onlineorals.$update(function() {
                  $location.path('onlineOrals');
                });
            } else {
                $scope.submitted = true;
            }
        };

         $scope.find = function() {
            OnlineOrals.query(function(onlineorals) {
                $scope.onlineorals = onlineorals;
            });
        };


        $scope.findOne = function() {
            OnlineOrals.get({
                onlineoralsId: $stateParams.onlineoralsId
            }, function(onlineorals) {
                $scope.onlineorals = onlineorals;
            });
        };
        $scope.findOneAssignment = function() {
            OnlineOrals.get({
                onlineoralsId: $stateParams.curriculumId
            }, function(onlineorals) {
                $scope.onlineorals = onlineorals;
            });
        };
        $scope.goToEdit = function(id){
            $location.path('/#!/onlineOrals/'+id);
        };
        $scope.goToAssign = function(id){
            $location.path('/#!/onlineOrals/assign'+id);
        };
        $scope.returnToPackageHome = function(){
            $location.path('/onlineOrals');
        };
    }
]);
