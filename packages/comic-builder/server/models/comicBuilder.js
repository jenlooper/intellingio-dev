'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * ComicBuilderElements Schema
 */
var ComicBuilderElementsSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    phrase: {
        type: String,
        default: '',
        trim: true
    },
    voice: {
        type: Boolean,
        default: false,
        trim:true
    },
    recordingUrl: {
        type: String,
        default: '',
        trim: true
    },
    
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});
/**
 * Comic Builder Schema
 */
var ComicBuilderSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    title: {
        type: String,
        default: '',
        trim: true
    },
    comicbuilderelements: [ComicBuilderElementsSchema],
    
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

/**
 * Validations
 */
/*ComicBuilderSchema.path('paragraph').validate(function(paragraph) {
    return paragraph.length;
}, 'Paragraph cannot be blank');
*/

/**
 * Statics
 */
ComicBuilderSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).populate('user', 'name username').exec(cb);
};

mongoose.model('Comic', ComicBuilderSchema);
