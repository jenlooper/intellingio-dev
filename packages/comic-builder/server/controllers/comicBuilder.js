'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    //fs = require('fs'),
    ComicBuilder = mongoose.model('Comic'),
    _ = require('lodash');


        



/**
 * Find paragraph by id
 */
exports.comicbuilder = function(req, res, next, id) {
    ComicBuilder.load(id, function(err,  comicbuilder) {
        if (err) return next(err);
        if (!comicbuilder) return next(new Error('Failed to load comicbuilder ' + id));
        req.comicbuilder = comicbuilder;
        next();
    });
};

/**
 * Create an article
 */
exports.create = function(req, res) {
    var comicbuilder = new ComicBuilder(req.body);
    comicbuilder.user = req.user;

    comicbuilder.save(function(err) {
        if (err) {
            return res.json(500, {
                error: 'Cannot save the comic'
            });
        }
        res.json(comicbuilder);

    });
};
/**
 * Show 
 */
exports.show = function(req, res) {

    res.jsonp(req.comicbuilder);
};

/**
 * Update a comicbuilder
 */
exports.update = function(req, res) {
    var comicbuilder = req.comicbuilder;
    
    
    //if we're sourcing the voice
    if (req.body.voice === true) {
    

        var length = 10;
        var myString = req.body.paragraph;
        var myTruncatedString = myString.substring(0,length);

        var title = myTruncatedString + '...';

        var postParams = {
            title: title,
            script: req.body.paragraph,
            test: 1,
            ping: ''
        };

        var querystring = require('querystring');
        var requestParams = querystring.stringify(postParams);
        var user = '18582';
        var token = '2da9a81b04a541db6e975431c46b9ab1';
        var auth = user + ':' + token;
        var host = 'api.voicebunny.com';
        var path = '/projects/addSpeedy';
        var https = require('https');
        var request = https.request('https://' + auth + '@' + host + path + '?' + requestParams, function(response) {
            response.setEncoding('utf8');
            response.on('data', function (chunk) {
            //process.stdout.write(chunk);
            var myURL = JSON.parse(chunk);
            
            var urlToSave = myURL.project.reads[0].urls.part001.default;
            console.log(urlToSave);
            
            comicbuilder = _.extend(comicbuilder, {comicbuilder:req.body.text,comicbuilderelements:req.body.comicbuilderelements,voice:true,recordingUrl:urlToSave});
            
            listeningcomp.save(function(err) {
                if (err) {
                    return res.send('users/signup', {
                        errors: err.errors,
                        comicbuilder: comicbuilder
                    });
                } else {
                    res.jsonp(comicbuilder);
                }
            });
            
            
        });
        response.on('error', function (e) {
            //console.log('e: ' + e);
            process.stdout.write(e);
        });
        

        });

        request.end();
        request.on('error', function(e) {
        //console.error(e);
        });

    }

    else 


    {

    comicbuilder = _.extend(comicbuilder, {text:req.body.text,comicbuilderelements:req.body.comicbuilderelements,voice:false,recordingUrl:''});
            
        comicbuilder.save(function(err) {
                if (err) {
                    return res.send('users/signup', {
                        errors: err.errors,
                        comicbuilder: comicbuilder
                    });
                } else {
                    console.log('sending');
                    res.jsonp(comicbuilder);
                }
            }); 
    }
    
};

/**
 * Delete comicbuilder
 */
exports.destroy = function(req, res) {

    var comicbuilder = req.comicbuilder;

    comicbuilder.remove(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                comicbuilder: comicbuilder
            });
        } else {
            res.jsonp(comicbuilder);
        }
    });
};

/**
 * List 
 */
exports.all = function(req, res) {


    ComicBuilder.find().sort('-created').populate('user', 'name username').exec(function(err, comicbuilder) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(comicbuilder);
        }
    });
};
