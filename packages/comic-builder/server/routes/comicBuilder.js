'use strict';

var lc = require('../controllers/comicBuilder');

// Article authorization helpers
var hasAuthorization = function(req, res, next) {
    if (!req.user.isAdmin && req.comicbuilder.user.id !== req.user.id) {
        return res.send(401, 'User is not authorized');
    }
    next();
};

module.exports = function(ComicBuilder, app, auth) {

    app.route('/comicBuilder')
    	.get(auth.requiresLogin, hasAuthorization, lc.all)
        .post(auth.requiresLogin, hasAuthorization, lc.create);    
    app.route('/comicBuilder/:comicbuilderId')
        .get(auth.requiresLogin, hasAuthorization, lc.show)
        .put(auth.requiresLogin, hasAuthorization, lc.update)
        .delete(auth.requiresLogin, hasAuthorization, lc.destroy);

    app.param('comicbuilderId', lc.comicbuilder);
};
