'use strict';

angular.module('mean').factory('ComicBuilder', ['$resource',
	function($resource) {
		return $resource('comicBuilder/:comicbuilderId', {
			comicbuilderId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

