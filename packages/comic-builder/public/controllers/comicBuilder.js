'use strict';

angular.module('mean').controller('ComicBuilderController', ['$scope', '$stateParams', '$location', 'Global', 'ComicBuilder',
    function($scope, $stateParams, $location, Global, ComicBuilder) {
        $scope.global = Global;

        $scope.hasAuthorization = function(comicbuilder) {
            if (!comicbuilder || !comicbuilder.user) return false;
            return $scope.global.isAdmin || comicbuilder.user._id === $scope.global.user._id;
        };


        $scope.create = function(isValid) {
            if (isValid) {
              $scope.cberror = 0;
                var comicbuilder = new ComicBuilder({
                    title: this.title,
                    comicbuilderElements: []
                });
                comicbuilder.$save(function(response) {
                    $location.path('comicBuilder/' + response._id);
                });

                this.title = '';

            } else {
                $scope.submitted = true;
                $scope.cberror = "Please give a title to this comic."
            }
        };

        $scope.remove = function(comicbuilder) {
            if (comicbuilder) {
                comicbuilder.$remove();

                for (var i in $scope.comicbuilder) {
                    if ($scope.comicbuilder[i] === comicbuilder) {
                        $scope.comicbuilder.splice(i, 1);
                    }
                }
            } else {
                $scope.comicbuilder.$remove(function(response) {
                    $location.path('comicBuilder/');
                });
            }
        };

        $scope.removeComicBuilderElement = function(comicbuilderElement) {
            for (var i in $scope.comicbuilder.comicbuilderelements) {
                if ($scope.comicbuilder.comicbuilderelements[i] === comicbuilderElement) {
                    $scope.comicbuilder.comicbuilderelements.splice(i, 1);
                    $scope.update(true);
                }
            }
        };

        $scope.update = function(isValid,getVoice) {
            if (isValid) {
                var comicbuilder = $scope.comicbuilder;
                if (!comicbuilder.updated) {
                    comicbuilder.updated = [];
                }
                comicbuilder.updated.push(new Date().getTime());
                comicbuilder.$update(function() {
                    $location.path('comicBuilder/' + comicbuilder._id);
                });
            } else {
                $scope.submitted = true;
            }
        };

         $scope.find = function() {
            ComicBuilder.query(function(comicbuilder) {
                $scope.comicbuilder = comicbuilder;
            });
        };

        $scope.add = function() {
            $scope.comicbuilder.comicbuilderelements.push({
                phrase: $scope.newPhrase,
            });
            $scope.update(true);

            $scope.newPhrase = '';
        };
        $scope.findOne = function() {
            ComicBuilder.get({
                comicbuilderId: $stateParams.comicbuilderId
            }, function(comicbuilder) {
                $scope.comicbuilder = comicbuilder;
            });
        };
         $scope.complete = function(){
            $location.path('/comicBuilder');
        };
    }
]);
