'use strict';

//Setting up route
angular.module('mean').config(['$stateProvider',
    function($stateProvider) {
        // Check if the user is connected
        var checkLoggedin = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0') $timeout(deferred.resolve);

                // Not Authenticated
                else {
                    $timeout(deferred.reject);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

        // states for my app
        
            $stateProvider
                .state('all comic-builder', {
                    url: '/comicBuilder',
                    templateUrl: 'comic-builder/views/list.html',
                    resolve: {
                        loggedin: checkLoggedin
                    }
                })
            .state('edit comic-builder', {
                url: '/comicBuilder/:comicbuilderId/edit',
                templateUrl: 'comic-builder/views/edit.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('create comic-builder', {
                url: '/comicBuilder/create',
                templateUrl: 'comic-builder/views/create.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('assign comic-builder', {
                url: '/comicBuilder/assign/:comicbuilderId',
                templateUrl: 'comic-builder/views/assign.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('comicbuilder by id', {
                url: '/comicBuilder/:comicbuilderId',
                templateUrl: 'comic-builder/views/view.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            });



            



    }
]);

