'use strict';

//Setting up route
angular.module('mean').config(['$stateProvider',
    function($stateProvider) {
        // Check if the user is connected
        var checkLoggedin = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0') $timeout(deferred.resolve);

                // Not Authenticated
                else {
                    $timeout(deferred.reject);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

        // states for my app

            $stateProvider
                .state('all listening-comp', {
                    url: '/listeningComp',
                    templateUrl: 'listening-comp/views/list.html',
                    resolve: {
                        loggedin: checkLoggedin
                    }
                })
            .state('edit listening-comp', {
                url: '/listeningComp/:curriculumId/edit',
                templateUrl: 'listening-comp/views/edit.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('create listening-comp', {
                url: '/listeningComp/create',
                templateUrl: 'listening-comp/views/create.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('assign listening-comp', {
                url: '/listeningComp/assign/:curriculumId',
                templateUrl: 'listening-comp/views/assign.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('create text', {
                url: '/listeningComp/teachers/:curriculumId',
                templateUrl: 'listening-comp/views/build.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('student view listeningcomp assignment', {
                url: '/listeningComp/students/assignment/2/:curriculumId',
                templateUrl: 'listening-comp/views/student-view.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            });







    }
]);
