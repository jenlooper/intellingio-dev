'use strict';

angular.module('mean').factory('ListeningComp', ['$resource',
	function($resource) {
		return $resource('listeningComp/:curriculumId', {
			curriculumId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

/*angular.module('mean').factory('audio',function ($document) {
  var audioElement = $document[0].createElement('audio'); // <-- Magic trick here
  return {
    audioElement: audioElement,

    play: function(filename) {
        audioElement.src = filename;
        audioElement.play();
    },
    pause: function(filename) {
        audioElement.src = filename;
        audioElement.pause();
    }

  }
});*/
