'use strict';

angular.module('mean').controller('ListeningCompController', ['$scope', '$sce', '$stateParams', '$location', 'Global', 'ListeningComp','audio',
    function($scope, $sce, $stateParams, $location, Global, ListeningComp,audio) {
        $scope.global = Global;

        $scope.hasAuthorization = function(listeningcomp) {
            if (!listeningcomp || !listeningcomp.user) return false;
            return $scope.global.isAdmin || listeningcomp.user._id === $scope.global.user._id;
        };

        $scope.trustSrc = function(src) {
            return $sce.trustAsResourceUrl(src);
        };
        $scope.create = function(isValid) {
            if (isValid) {
              $scope.lcerror = 0;

                var listeningcomp = new ListeningComp({
                    title: this.title,
                    paragraph: this.paragraph,
                    recordingUrl: this.recordingUrl,
                    listeningcompelements:[]
                });
                listeningcomp.$save(function(response) {
                    $location.path('listeningComp/teachers/' + response._id);
                });

                this.title = '';
                this.paragraph = '';
                this.recordingUrl = '';
                this.show_text = '';
            } else {
                $scope.submitted = true;
                $scope.lcerror = "Please give this exercise a title.";

            }
        };

        $scope.remove = function(listeningcomp) {
            if (listeningcomp) {
                listeningcomp.$remove();

                for (var i in $scope.listeningcomp) {
                    if ($scope.listeningcomp[i] === listeningcomp) {
                        $scope.listeningcomp.splice(i, 1);
                    }
                }
            } else {
                $scope.listeningcomp.$remove(function(response) {
                    $location.path('listeningComp/');
                });
            }
        };

         $scope.removeListeningCompElement = function(listeningcompElement) {
            for (var i in $scope.listeningcomp.listeningcompelements) {
                if ($scope.listeningcomp.listeningcompelements[i] === listeningcompElement) {
                    $scope.listeningcomp.listeningcompelements.splice(i, 1);
                    $scope.update(true);
                }
            }
        };

        $scope.update = function(isValid,go) {
            if (isValid) {
              $scope.perror = 0;
                var listeningcomp = $scope.listeningcomp;
                if (!listeningcomp.updated) {
                    listeningcomp.updated = [];
                }
                listeningcomp.updated.push(new Date().getTime());
                listeningcomp.$update(function() {
                  if (go){
                    $location.path('/listeningComp');
                  }
                });
            } else {
              $scope.perror = "Please add a text.";
                $scope.submitted = true;
            }
        };

         $scope.find = function() {
            ListeningComp.query(function(listeningcomp) {
                $scope.listeningcomp = listeningcomp;
            });
        };

        $scope.add = function() {
            $scope.listeningcomp.listeningcompelements.push({
                question: $scope.newQuestion,
            });
            $scope.update(true);
            $scope.newQuestion = '';
        };
        
        $scope.findOne = function() {
            ListeningComp.get({
                curriculumId: $stateParams.curriculumId
            }, function(listeningcomp) {
                $scope.listeningcomp = listeningcomp;
            });
        };

        /*$scope.complete = function(){
            $location.path('/listeningComp');
        };*/
        $scope.goToEdit = function(id){
            $location.path('/#!/listeningComp/'+id);
        };
        $scope.goToAssign = function(id){
            $location.path('/#!/listeningComp/assign'+id);
        };
        $scope.returnToPackageHome = function(){
            $location.path('/listeningComp');
        };
    }
]);
