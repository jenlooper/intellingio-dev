'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var ListeningComp = new Module('listening-comp');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
ListeningComp.register(function(app, auth, database) {

    //We enable routing. By default the Package Object is passed to the routes
    ListeningComp.routes(app, auth, database);

    //We are adding a link to the main menu for all authenticated users
    ListeningComp.menus.add({
        title: 'Comprehension',
        link: 'all listening-comp',
        icon: 'glyphicon glyphicon-headphones',
        roles: ['teacher'],
        menu: 'curriculum'
    });



    /**
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    ListeningComp.settings({
        'someSetting': 'some value'
    }, function(err, settings) {
        //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    ListeningComp.settings({
        'anotherSettings': 'some value'
    });

    // Get settings. Retrieves latest saved settigns
    ListeningComp.settings(function(err, settings) {
        //you now have the settings object
    });
    */

    return ListeningComp;
});
