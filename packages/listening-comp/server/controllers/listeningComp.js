'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    //fs = require('fs'),
    ListeningComp = mongoose.model('ListeningComp'),
    _ = require('lodash');


/**
 * Find paragraph by id
 */
exports.listeningcomp = function(req, res, next, id) {
    ListeningComp.load(id, function(err, listeningcomp) {
        if (err) return next(err);
        if (!listeningcomp) return next(new Error('Failed to load listeningcomp ' + id));
        req.listeningcomp = listeningcomp;
        next();
    });
};

exports.create = function(req, res) {

    var listeningcomp = new ListeningComp(req.body);

    listeningcomp.user = req.user;

    /*if (req.body.voice_voicebunny === true) {


        var postParams = {
            title: req.body.title,
            script: req.body.paragraph,
            test: 1,
            ping: ''
        };

        var querystring = require('querystring');
        var requestParams = querystring.stringify(postParams);
        var user = '18582';
        var token = '2da9a81b04a541db6e975431c46b9ab1';
        var auth = user + ':' + token;
        var host = 'api.voicebunny.com';
        var path = '/projects/addSpeedy';
        var https = require('https');
        var request = https.request('https://' + auth + '@' + host + path + '?' + requestParams, function(response) {
            response.setEncoding('utf8');
            response.on('data', function (chunk) {
            //process.stdout.write(chunk);
            var myURL = JSON.parse(chunk);

            var urlToSave = myURL.project.reads[0].urls.part001.default;
            console.log(urlToSave);

            listeningcomp = _.extend(listeningcomp, {title:req.body.title,paragraph:req.body.paragraph,listeningcompelements:req.body.listeningcompelements,voice_voicebunny:true,voicebunny_recordingUrl:urlToSave});

            listeningcomp.save(function(err) {
                if (err) {
                    return res.send('users/signup', {
                        errors: err.errors,
                        listeningcomp: listeningcomp
                    });
                } else {
                    res.jsonp(listeningcomp);
                }
            });


        });
        response.on('error', function (e) {
            //console.log('e: ' + e);
            process.stdout.write(e);
        });


        });

        request.end();
        request.on('error', function(e) {
        //console.error(e);
        });

    }*/


    listeningcomp.save(function(err) {

        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                listeningcomp: listeningcomp
            });
        } else {
            res.jsonp(listeningcomp);

      }
    });
};

/**
*Get voice
*/
exports.registerAudio = function(req, res) {

  var listeningcomp = _.extend(listeningcomp, {recordingUrl:'test'});
  console.log(listeningcomp);

  listeningcomp.save(function(err) {
          if (err) {
              return res.send('users/signup', {
                  errors: err.errors,
                  listeningcomp: listeningcomp
              });
          } else {
              res.jsonp(listeningcomp);
          }
      });

};

/**
 * Show
 */
exports.show = function(req, res) {

    res.jsonp(req.listeningcomp);
};

/**
 * Update
 */
exports.update = function(req, res) {
    var listeningcomp = req.listeningcomp;

    //if we're sourcing the voice
    /*if (req.body.voice_voicebunny === true) {

        var postParams = {
            title: req.body.title,
            script: req.body.paragraph,
            test: 1,
            ping: ''
        };

        var querystring = require('querystring');
        var requestParams = querystring.stringify(postParams);
        var user = '18582';
        var token = '2da9a81b04a541db6e975431c46b9ab1';
        var auth = user + ':' + token;
        var host = 'api.voicebunny.com';
        var path = '/projects/addSpeedy';
        var https = require('https');
        var request = https.request('https://' + auth + '@' + host + path + '?' + requestParams, function(response) {
            response.setEncoding('utf8');
            response.on('data', function (chunk) {
            //process.stdout.write(chunk);
            var myURL = JSON.parse(chunk);

            var urlToSave = myURL.project.reads[0].urls.part001.default;
            console.log(urlToSave);

            listeningcomp = _.extend(listeningcomp, {title:req.body.title,paragraph:req.body.paragraph,listeningcompelements:req.body.listeningcompelements,voice_voicebunny:true,voicebunny_recordingUrl:urlToSave});

            listeningcomp.save(function(err) {
                if (err) {
                    return res.send('users/signup', {
                        errors: err.errors,
                        listeningcomp: listeningcomp
                    });
                } else {
                    console.log('sending');
                    res.jsonp(listeningcomp);
                }
            });


        });
        response.on('error', function (e) {
            //console.log('e: ' + e);
            process.stdout.write(e);
        });


        });

        request.end();
        request.on('error', function(e) {
        //console.error(e);
        });

    }

    else


    {*/

    listeningcomp = _.extend(listeningcomp, {
        title:req.body.title,
        paragraph:req.body.paragraph,
        listeningcompelements:req.body.listeningcompelements,
        recordingUrl:req.body.recordingUrl
    });

        listeningcomp.save(function(err) {
                if (err) {
                    return res.send('users/signup', {
                        errors: err.errors,
                        listeningcomp: listeningcomp
                    });
                } else {
                    res.jsonp(listeningcomp);
                }
            });
    ////}

};

/**
 * Delete listeningcomp
 */
exports.destroy = function(req, res) {

    var listeningcomp = req.listeningcomp;

    listeningcomp.remove(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                listeningcomp: listeningcomp
            });
        } else {
            res.jsonp(listeningcomp);
        }
    });
};

/**
 * List
 */
exports.all = function(req, res) {


    ListeningComp.find().sort('-created').populate('user', 'name username').exec(function(err, listeningcomp) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(listeningcomp);
        }
    });
};
