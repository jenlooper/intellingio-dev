'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * ListeningCompElements Schema
 */
var ListeningCompElementsSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    question: {
        type: String,
        default: '',
        trim: true
    },
    answer: {
        type:String,
        default: '',
        trim:true
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});
/**
 * Listening Comp Schema
 */
var ListeningCompSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    paragraph: {
        type: String,
        default: '',
        trim: true
    },
    title: {
        type: String,
        default: '',
        trim: true
    },
    show_text: {
        type: Boolean,
        default: false,
        trim:true
    },
    recordingUrl: {
        type: String,
        default: '',
        trim: true
    },
    listeningcompelements: [ListeningCompElementsSchema],

    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

/**
 * Validations
 */
ListeningCompSchema.path('title').validate(function(title) {
    return title.length;
}, 'Title cannot be blank');



/**
 * Statics
 */
ListeningCompSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).populate('user', 'name username').exec(cb);
};

mongoose.model('ListeningComp', ListeningCompSchema);
