'use strict';

var lc = require('../controllers/listeningComp');

var hasAuthorization = function(req, res, next) {
    if (!req.user.isAdmin && req.listeningcomp.user.id !== req.user.id) {
        return res.send(401, 'User is not authorized');
    }
    next();
};

module.exports = function(ListeningComp, app, auth) {

    app.route('/listeningComp')
    	.get(auth.requiresLogin, hasAuthorization, lc.all)
        .post(auth.requiresLogin, hasAuthorization, lc.create);
    app.route('/listeningComp/:listeningcompId')
        .get(auth.requiresLogin, hasAuthorization, lc.show)
        .put(auth.requiresLogin, hasAuthorization, lc.update)
        .delete(auth.requiresLogin, hasAuthorization, lc.destroy);
    app.route('/listeningComp/register_audio')
      .post(auth.requiresLogin, hasAuthorization, lc.registerAudio);



    app.param('listeningcompId', lc.listeningcomp);
};
