'use strict';

//Global service for global variables
angular.module('mean.system').factory('Global', [

    function() {
        var _this = this;
        _this._data = {
            user: window.user,
            authenticated: false,
            isAdmin: false,
            isStudent: false,
            isTeacher: false
        };
        if (window.user && window.user.roles) {
            _this._data.authenticated = window.user.roles.length;
            _this._data.isAdmin = window.user.roles.indexOf('admin') !== -1;
            _this._data.isStudent = window.user.roles.indexOf('student') !== -1;
            _this._data.isTeacher = window.user.roles.indexOf('teacher') !== -1;
        }
        return _this._data;
    }
]);
