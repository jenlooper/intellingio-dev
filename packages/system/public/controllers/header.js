'use strict';

angular.module('mean.system').controller('HeaderController', ['$scope', '$location', '$anchorScroll', '$rootScope', 'Global', 'Menus',
    function($scope, $location, $anchorScroll, $rootScope, Global, Menus) {
        $scope.global = Global;
        $scope.menus = {};

         

        // Default hard coded menu items for main menu
        var defaultMainMenu = [];
        var teacherMenu = [];
        var studentMenu = [];
        var adminMenu = [];
        var curriculumMenu = [];

        $scope.gotoAnchor = function(id) {
            $location.hash(id);
            $anchorScroll();
        };
    

        // Query menus added by modules. Only returns menus that user is allowed to see.
        function queryMenu(name, defaultMenu) {

            Menus.query({
                name: name,
                defaultMenu: defaultMenu
                }, function(menu) {
                $scope.menus[name] = menu;
            });
        }

        // Query server for menus and check permissions
        queryMenu('main', defaultMainMenu);
        queryMenu('teacher', teacherMenu);
        queryMenu('student', studentMenu);
        queryMenu('admin', adminMenu);
        queryMenu('curriculum', curriculumMenu);
        

        $scope.isCollapsed = false;

        $rootScope.$on('loggedin', function() {

            queryMenu('main', defaultMainMenu);
            queryMenu('teacher', teacherMenu);
            queryMenu('student', studentMenu);
            queryMenu('admin', adminMenu);
            queryMenu('curriculum', curriculumMenu);

            $scope.global = {
                authenticated: !! $rootScope.user,
                user: $rootScope.user
            };
        });



    }


]);
