'use strict';

module.exports = {
  forgot_password_email: function(user, req, token, mailOptions) {
    mailOptions.html = [
      'Hi ' + user.name + ',<br/>',
      'We have received a request to reset the password for your account.',
      'If you made this request, please click on the link below or paste this into your browser to complete the process:',
      'http://' + req.headers.host + '/#!/reset/' + token,
      'This link will work for 1 hour or until your password is reset.',
      'If you did not ask to change your password, please ignore this email and your account will remain unchanged.'
    ].join('\n\n');
    mailOptions.subject = 'Resetting your Intelling.io password';
    return mailOptions;
  },
  contact_us_email: function(req, mailOptions) {
    mailOptions.html = [
      'From: '+req.body.email+'<br/>Message:'+req.body.message+'.'
    ].join('\n\n');
    mailOptions.subject = 'Contact Us';
    return mailOptions;
  },
  register_confirmation_email: function(req, mailOptions) {
    console.log("mailing");
    mailOptions.html = [
      'Hi and welcome to Intelling.io!<br/><br/>',
      'We are happy that you are joining our community. We will activate your account and send you instructions shortly on how to build your student roster.<br/><br/>',
      'Your class code is ' + req.body.secret_code + '. Please keep this code for reference. Your students will use it to associate themselves to your roster.'
    ].join('\n\n');
    mailOptions.subject = 'Welcome to Intelling.io';
    return mailOptions;
  },
  invite_student: function(req, mailOptions) {
    console.log(req.body);
    mailOptions.html = [
      'Welcome to Intelling.io!<br/><br/>',
      'Your teacher is inviting you to join class ' + req.body.classname + '.<br/><br/>',
      'Please visit http://intelling.io/#!/auth/register?classid='+ req.body.classcode + ' or copy and paste that link into your browser and complete registration.<br/><br/>',
      'We\'ll get you all set up to start learning!'
    ].join('\n\n');
    mailOptions.subject = 'Welcome to Intelling.io';
    return mailOptions;
  }
};
