'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


/**
 * Class Management Schema
 */
var ClassManagementSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    classname: {
        type: String,
        default: '',
        trim: true
    },
    classcode: {
        type: String,
        default: '',
        trim: true
    },
    students: [],
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

/**
 * Validations
 */
ClassManagementSchema.path('classname').validate(function(classname) {
    return classname.length;
}, 'Paragraph cannot be blank');


/**
 * Statics
 */
ClassManagementSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).populate('user', 'name username').exec(cb);
};

mongoose.model('Classes', ClassManagementSchema);
