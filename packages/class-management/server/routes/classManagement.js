'use strict';

var cm = require('../controllers/classManagement');

// Article authorization helpers
var hasAuthorization = function(req, res, next) {
    if (!req.user.isAdmin && req.classmanagement.user.id !== req.user.id) {
        return res.send(401, 'User is not authorized');
    }
    next();
};

module.exports = function(ClassManagement, app, auth) {

    app.route('/classManagement')
    	.get(auth.requiresLogin, hasAuthorization, cm.all)
        .post(auth.requiresLogin, hasAuthorization, cm.create);    
    app.route('/classManagement/:edclassId')
        .get(auth.requiresLogin, hasAuthorization, cm.show)
        .put(auth.requiresLogin, hasAuthorization, cm.update)
        .delete(auth.requiresLogin, hasAuthorization, cm.destroy);

    app.param('edclassId', cm.classmanagement);
};

