'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    //fs = require('fs'),
    ClassManagement = mongoose.model('Classes'),
    _ = require('lodash');


/**
 * Find by id
 */
exports.classmanagement = function(req, res, next, id) {
    ClassManagement.load(id, function(err, edclass) {
        if (err) return next(err);
        if (!edclass) return next(new Error('Failed to load edclass ' + id));
        req.edclass = edclass;
        next();
    });
};

exports.create = function(req, res) {

    var edclass = new ClassManagement(req.body);
    
    edclass.user = req.user;

    edclass.save(function(err) {

        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                edclass: edclass
            });
        } else {
            res.jsonp(edclass);

      }
    });
};


/**
 * Show 
 */
exports.show = function(req, res) {

    res.jsonp(req.edclass);
};

/**
 * Update a class
 */
exports.update = function(req, res) {
    var edclass = req.edclass;
    
    edclass = _.extend(edclass, req.body);
       
    edclass.save(function(err) {
                if (err) {
                    return res.send('users/signup', {
                        errors: err.errors,
                        edclass: edclass
                    });
                } else {
                    res.jsonp(edclass);
                }
            });   
};


/**
 * Delete class
 */
exports.destroy = function(req, res) {

    var edclass = req.edclass;

    edclass.remove(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                edclass: edclass
            });
        } else {
            res.jsonp(edclass);
        }
    });
};

/**
 * List 
 */
exports.all = function(req, res) {


    ClassManagement.find().sort('-created').populate('user', 'name username').exec(function(err, edclass) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(edclass);
        }
    });
};
