'use strict';

angular.module('mean').factory('ClassManagement', ['$resource',
	function($resource) {
		return $resource('classManagement/:edclassId', {
			edclassId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

