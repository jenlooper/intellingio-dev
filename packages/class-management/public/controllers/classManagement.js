'use strict';

angular.module('mean').controller('ClassManagementController', ['$scope', '$stateParams', '$location', 'Global', 'ClassManagement','Users','Assignment','$http',
    function($scope, $stateParams, $location, Global, ClassManagement,Users,Assignment,$http) {
        $scope.global = Global;
        $scope.curricula = [];

        $scope.hasAuthorization = function(edclass) {
            if (!edclass || !edclass.user) return false;
            return $scope.global.isAdmin || edclass.user._id === $scope.global.user._id;
        };

        $scope.create = function(isValid) {

            if (isValid) {
                var edclass = new ClassManagement({
                    classname: this.classname,
                    classcode: uuid.v4(),
                    students:[]
                });
                edclass.$save(function(response) {
                     $location.path('/teachers/classes');                     
                });

                this.classname = '';
            } else {
                $scope.submitted = true;
            }
        };

        
        
        $scope.assign = function(isValid,edclass,curriculumId,curriculumTitle,curriculumType,curriculumTypeId,content) {

            var postData = {
                curriculumId: curriculumId,
                edClass: edclass
            };
            var okToAssign = false;
            $http.post('/assignmentsCheck', postData).then(
                function(response) {
                    if (isValid && !response.data.exists) {
                        //does this assignment exist?
                        for (var i in edclass.students) {
                        var assignment = new Assignment({
                            curriculumId:curriculumId,
                            studentId: edclass.students[i],
                            curriculumType:curriculumType,
                            curriculumTypeId:curriculumTypeId,
                            curriculumName:curriculumTitle,
                            edClass:edclass,
                            complete:false,
                            responses:content

                        });
                        assignment.$save(function(response) {
                            });
                        }
                        
                        $scope.assignsuccess = 'This assignment has been successfully assigned!';
                    } else {
                        //alert('Already assigned.');
                        $scope.assignerror = 'This assignment has already been assigned to this class.';
                        $scope.submitted = true;
                    }
                },
                function(response) {
                });
        };



        $scope.remove = function(edclass) {
            if (edclass) {
                edclass.$remove();

                for (var i in $scope.edclasses) {
                    if ($scope.edclasses[i] === edclass) {
                        $scope.edclasses.splice(i, 1);
                    }
                }
            } else {
                $scope.edclass.$remove(function(response) {
                    //$location.path('classManagement/');
                });
            }
        };

        $scope.update = function(isValid) {
            if (isValid) {
                var edclass = $scope.edclass;
                if (!edclass.updated) {
                    edclass.updated = [];
                }
                edclass.updated.push(new Date().getTime());
                edclass.$update(function() {
                    //$location.path('classManagement/' + edclass._id);
                });
            } else {
                $scope.submitted = true;
            }
        };

         $scope.find = function() {
            ClassManagement.query(function(edclasses) {
                $scope.edclasses = edclasses;
                console.log($scope.edclasses)
            });

        };

        $scope.findStudents = function() {
            /*Users.query(function(users) {
                $scope.userType = 'student';
                $scope.users = users.filter(function(students){
                    return(students.roles.indexOf($scope.userType) !== -1);
                });
            });*/            
            
        };

        

        $scope.addStudent = function(sid) {
            ClassManagement.get({
                edclassId: $stateParams.edclassId
            }, function(edclass) {
                $scope.edclass = edclass;
                $scope.edclass.students.push({
                    studentId: sid
                });
                $scope.update(true);
                $scope.studentId = '';
            });

        };


        $scope.findOne = function() {
            ClassManagement.get({
                edclassId: $stateParams.edclassId
            }, function(edclass) {
                $scope.edclass = edclass;
            });


        };

        $scope.getClasses = function() {
            ClassManagement.query(function(edclasses) {
                $scope.edclasses = edclasses;
                $scope.selectedClass = $scope.edclasses[0];
            });
        };


       $scope.addRemoveStudent = function(student,studentId) {

        var classIdx = $scope.edclasses.indexOf($scope.selectedClass);

        //console.log($scope.edclasses[classIdx].students.indexOf(studentId));

            if ($scope.edclasses[classIdx].students.indexOf(studentId) === -1){
                $scope.edclasses[classIdx].students.push(studentId);
                $scope.edclass = $scope.edclasses[classIdx];
                $scope.update(true);
            }
            else{
                $scope.edclasses[classIdx].students.splice(studentId,1);
                $scope.edclass = $scope.edclasses[classIdx];
                $scope.update(true);
            }

        };

        $scope.complete = function(){
            $location.path('/classManagement');
        };
    }
]);
